﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace SocketUdpClient
{
    class Program
    {
        private static ushort _localPort;
        private static ushort _remotePort = 30000;
        private static Socket _listeningSocket;
        private static EndPoint _remotePoint;

        private static ushort _broadcastPort = 30002;

        static void Main(string[] args)
        {

            _localPort = (ushort)new Random().Next(30003, 32000);
            Console.WriteLine("client port: {0}", _localPort);
            
            if (args.Length >= 2)
            {
                try
                {
                    _remotePoint = new IPEndPoint(IPAddress.Parse(args[0]), Convert.ToUInt16(args[1]));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    _remotePoint = GetLocalIPAddress(_remotePort);
                }
            }
            else
            {
                Console.Write("Server IP: ");
                
                IPAddress ipAddress;

                if (IPAddress.TryParse(Console.ReadLine(), out ipAddress))
                {
                    _remotePoint = new IPEndPoint(ipAddress, _remotePort);
                }
                else
                {
                    _remotePoint = GetLocalIPAddress(_remotePort);
                }
            }

            try
            {
                _listeningSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                Task listeningTask = new Task(Listen);
                listeningTask.Start();
                Console.WriteLine();
                Random random = new Random();
                Thread.Sleep(200);

                while (true)
                {
                    string message =
                        new StringBuilder()
                            .Append(GenerateRandomText(random.Next(0, 23)))
                            .Append(GenerateRandomText(random.Next(0, 23)))
                            .Append(GenerateRandomText(random.Next(0, 23)))
                            .Append(GenerateRandomText(random.Next(0, 23)))
                            .Append(GenerateRandomText(random.Next(0, 23)))
                            .ToString();

                    Thread.Sleep(50);
                    byte[] data = Encoding.Unicode.GetBytes(message);
                    
                    _listeningSocket.SendTo(data, _remotePoint);
                }
            }
            catch (SocketException sEx)
            {
                Console.WriteLine(sEx.Message);
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
            finally
            {
                Close();
            }
        }

        private static IPEndPoint GetLocalIPAddress(ushort portNumber)
        {
            IPAddress ipAddress =
                            Dns.GetHostEntry(Dns.GetHostName())
                            .AddressList
                            .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork)
                            ?? IPAddress.Parse("127.0.0.1");

            return new IPEndPoint(ipAddress, portNumber);
        }

        private static void Listen()
        {
            try
            {
                _listeningSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
                IPEndPoint iep = new IPEndPoint(IPAddress.Any, 30002);
                _listeningSocket.Bind(iep);

                //IPEndPoint localIP = GetLocalIPAddress(_localPort);
                //_listeningSocket.Bind(localIP);

                _listeningSocket.SendTo(Encoding.Unicode.GetBytes("Connected"), _remotePoint);
                Console.WriteLine("Connected to {0}", _remotePoint);

                while (true)
                {
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    byte[] data = new byte[256];

                    EndPoint remoteIp = new IPEndPoint(IPAddress.Any, 0);

                    do
                    {
                        bytes = _listeningSocket.ReceiveFrom(data, ref remoteIp);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (_listeningSocket.Available > 0);
                    
                    IPEndPoint remoteFullIp = remoteIp as IPEndPoint;
                    
                    Console.WriteLine("{0}:{1} - {2}", remoteFullIp.Address, remoteFullIp.Port, builder);
                }
            }
            catch (SocketException sEx)
            {
                Console.WriteLine(sEx.Message);
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
            finally
            {
                Close();
            }
        }

        private static void Close()
        {
            if (_listeningSocket != null)
            {
                _listeningSocket.SendTo(Encoding.Unicode.GetBytes("Disconnected"), _remotePoint);
                _listeningSocket.Shutdown(SocketShutdown.Send);
                _listeningSocket.Close();
                _listeningSocket = null;
                Console.WriteLine("Client turn off");
            }
        }

        private static string GenerateRandomText(int index)
        {
            string[] words = new[]
            {
                " This",
                " happens",
                " because",
                " you're",
                " adding",
                " multiple",
                " paragraphs",
                " to",
                " the",
                " range",
                " after",
                " the",
                " range",
                " it",
                " seems",
                " that",
                " setting",
                " the",
                " Text",
                " property",
                " is",
                " equivalent",
                " to"
            };

            return words[index];
        }
    }
}