﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ServerSocketUdp
{
    class Program
    {
        private static ushort _reciverLocalPort = 30000;
        private static ushort _senderLocalPort = 30001;
        private static Socket _listeningSocket;
        private static Socket _senderSocket;

        private static Socket _broadcastSocket;
        private static IPEndPoint _broadcastIP;

        private static IPEndPoint _localIP;

        static void Main(string[] args)
        {
            try
            {
                if (args.Length >= 2)
                {
                    try
                    {
                        _localIP = new IPEndPoint(IPAddress.Parse(args[0]), Convert.ToUInt16(args[1]));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    finally
                    {
                        _localIP = GetLocalIPAddress(_reciverLocalPort);
                    }
                }

                _localIP = GetLocalIPAddress(_reciverLocalPort);

                PrepareSenderSocket();
                PrepareBroadcastSocket();

                Task listeningTask = new Task(Listen);
                listeningTask.Start();
            }
            catch (SocketException sEx)
            {
                Console.WriteLine(sEx.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Close(_listeningSocket, SocketShutdown.Both, "Server turn off");
            }

            Console.ReadKey();
        }

        private static IPEndPoint GetLocalIPAddress(ushort portNumber)
        {
            IPAddress ipAddress =
                            Dns.GetHostEntry(Dns.GetHostName())
                            .AddressList
                            .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork)
                            ?? IPAddress.Parse("127.0.0.1");

            return new IPEndPoint(ipAddress, portNumber);
        }

        private static void PrepareSenderSocket()
        {
            _senderSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint senderIP = GetLocalIPAddress(_senderLocalPort);
            _senderSocket.Bind(senderIP);
        }

        private static void PrepareBroadcastSocket()
        {
            _broadcastSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _broadcastIP = new IPEndPoint(IPAddress.Broadcast, 30002);
            _broadcastSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
        }

        private static void PrepareLisenerSocket()
        {
            _listeningSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _listeningSocket.Bind(_localIP);
        }

        private static void Listen()
        {
            try
            {
                Console.WriteLine("Server info: {0}:{1}", _localIP.Address, _localIP.Port);
                Console.WriteLine("Server RUN");

                List<IPEndPoint> clients = new List<IPEndPoint>();

                PrepareLisenerSocket();

                while (true)
                {
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    byte[] data = new byte[256];

                    EndPoint remoteIp = new IPEndPoint(IPAddress.Any, 0);
                    do
                    {
                        bytes = _listeningSocket.ReceiveFrom(data, ref remoteIp);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));

                        IPEndPoint remoteIP = remoteIp as IPEndPoint;

                        if (remoteIP == null)
                        {
                            Console.WriteLine(remoteIp);
                            continue;
                        }

                        //AddClinet(clients, remoteIP);
                    }
                    while (_listeningSocket.Available > 0);

                    IPEndPoint remoteFullIp = remoteIp as IPEndPoint;

                    Console.WriteLine("{0}:{1} - {2}", remoteFullIp.Address, remoteFullIp.Port, builder);

                    //if (clients.Count > 1)
                    //{
                    //    Task.Factory.StartNew(() => SendMessageToAllUserWithoutReciver(clients, data, remoteFullIp));
                    //}
                    Task.Factory.StartNew(() => SendBroadcastMessage(data));
                }
            }
            catch (SocketException sEx)
            {
                Console.WriteLine(sEx.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Close(_listeningSocket, SocketShutdown.Both, "Server turn off");
            }
        }

        private static void AddClinet(IList<IPEndPoint> clients, IPEndPoint remoteIP)
        {
            if (clients.Count == 0)
            {
                clients.Add(remoteIP);
            }
            else
            {
                bool isContains = false;
                for (int i = 0, len = clients.Count; i < len; i++)
                {
                    if (clients[i].Equals(remoteIP))
                    {
                        isContains = true;
                    }
                }

                if (!isContains)
                {
                    clients.Add(remoteIP);
                }
            }
        } 

        private static void SendMessageToAllUserWithoutReciver(IEnumerable<IPEndPoint> clients, byte[] data, IPEndPoint remoteFullIp)
        {
            try
            {
                foreach (IPEndPoint client in clients)
                {
                    if (!client.Equals(remoteFullIp))
                    {
                        _senderSocket.SendTo(data, client);
                    }
                }
            }
            catch (SocketException sEx)
            {
                Console.WriteLine(sEx.Message);
                Close(_senderSocket, SocketShutdown.Both, "Sender was close");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Close(_senderSocket, SocketShutdown.Both, "Sender was close");
            }
        }

        private static void SendBroadcastMessage(byte[] data)
        {
            _broadcastSocket.SendTo(data, _broadcastIP);
        }

        private static void Close(Socket socket, SocketShutdown shutdown, string message)
        {
            if (socket != null)
            {
                socket.Shutdown(shutdown);
                socket.Close();
                socket = null;
                Console.WriteLine(message);
            }
        }
    }
}
